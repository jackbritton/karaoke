#!/usr/bin/octave-cli

% Read.
disp('Reading from disc..');
[X, fs] = audioread('in.wav');
X = X';
m = columns(X);
n = rows(X);

% Centering and whitening.
disp('Whitening data..');
X_means = mean(X, n);
[E, D] = eig(cov(X*X'));
rotate = E';
scale = diag(diag(D).^-0.5);

% TODO D contains 0s.

X = inv(rotate)*scale*rotate*(X-X_means);

% Functions to optimize.
function out = g(in)
    out = tanh(in);
end
function out = g_prime(in)
    out = 1 - tanh(in).^2;
end

disp('Optimizing..');

% Weights.
c = 1;
W = eye(n, n);

% Optimize.
for p = 1:c
    disp(sprintf('%d of %d..', p, c));
    w = randn(n,1);
    w /= norm(w);
    while (1)
        w_next = 1/m*(X*g(w'*X)') - 1/m*(g_prime(w'*X)*ones(m,1))*w;
        % Decorrelate. TODO Try alternatives.
        for j = 1:p-1
            w_next -= w'*W(:,j)*W(:,j);
        end
        w_next /= norm(w_next);
        % Pointing in the same direction?
        if (abs(abs(w'*w_next) - 1) < 0.001)
            w = w_next;
            break;
        endif
        w = w_next;
    endwhile
    W(:,p) = w;
end

S = inv(rotate)*inv(scale)*rotate*W'*(X+W'*X_means);

% Write tracks to disc.
disp('Writing to disc..');
for p = 1:c
    audiowrite(sprintf('out%d.wav', p), S(p,:)', fs);
end
