This is a basic implementation of a Fast ICA for source separation. Here's some reading: http://www.cs.helsinki.fi/u/ahyvarin/papers/NN00new.pdf.

The octave script takes a 2-channel audio file, "in.wav", and tries to separate sources. Try it on some music, for some songs it's very cool.